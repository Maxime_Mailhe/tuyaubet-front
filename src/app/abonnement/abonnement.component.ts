import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faLock } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-abonnement',
  templateUrl: './abonnement.component.html',
  styleUrls: ['./abonnement.component.scss']
})

export class AbonnementComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit(): void {}

  lock = faLock;

}

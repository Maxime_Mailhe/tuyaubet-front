import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireInfoComponent } from './formulaire-info.component';

describe('FormulaireInfoComponent', () => {
  let component: FormulaireInfoComponent;
  let fixture: ComponentFixture<FormulaireInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaireInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AppComponent } from './../app.component';
import { AccueilComponent } from './../accueil/accueil.component';
import { NotificationService } from './../services/notification.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()

@Component({
  selector: 'app-formulaire-info',
  templateUrl: './formulaire-info.component.html',
  styleUrls: ['./formulaire-info.component.scss']
})
export class FormulaireInfoComponent implements OnInit {
  

  constructor(private userService: UserService,
              private router: Router,
              private app: AppComponent) {
               }
              
   url = 'https://tuyaubet.herokuapp.com';

  ngOnInit() {
    setTimeout(() => {
      this.app.hideMenu();
    },0)
  }

  submitInfo(form: NgForm)  {
    const email_utilisateur = form.value['email_utilisateur'];
    const sport_info = form.value['sport_info'];
    const evenement_info = form.value['evenement_info'];
    const commentaire_info = form.value['commentaire_info'];
    const lien_info = form.value['lien_info'];
    this.userService.addInfo(email_utilisateur,
            sport_info,
            evenement_info,
            commentaire_info,
            lien_info);
    this.userService.emailUsers.splice(0);
    this.userService.emailUsers.push(email_utilisateur);
    // this.userService.saveEmailUserToServer();
    // this.userService.saveInfosToServer();
    // this.router.navigate(['/abonnement']);
  }
  redirectFAQ(){
    this.router.navigate(['/pourquoi']);
  }
}

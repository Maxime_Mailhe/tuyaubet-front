import { SEOService } from './services/SEO.service';
import { NotificationService } from './services/notification.service';
import { NgModule } from '@angular/core';
import { BrowserModule, Meta } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormulaireInfoComponent } from './formulaire-info/formulaire-info.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthService} from './services/auth.service';
import { AuthComponent } from './auth/auth.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { UserService } from './services/user.service';
import { OfficeComponent } from './office/office.component';
import { HttpClientModule} from "@angular/common/http";
import { AbonnementComponent } from './abonnement/abonnement.component';
import { AccueilComponent } from './accueil/accueil.component';
import { PourquoiComponent } from './pourquoi/pourquoi.component';
import { CguComponent } from './cgu/cgu.component';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { ContactComponent } from './contact/contact.component';

const appRoutes: Routes = [
  { path:'info',
   component: FormulaireInfoComponent,
   data: {
     title: 'Mon supertuyau',
     description: " Transmets-le et gagne un freebet.",
     ogUrl: '/info'
   }},
  { path:'office', component: OfficeComponent},
  { path:'pourquoi',
   component: PourquoiComponent,
   data: {
     title: 'FAQ',
     description: " Foire Aux Questions.",
     ogUrl: '/pourquoi'
   }},
  { path: 'auth', component: AuthComponent},
  { path: 'abonnement', component: AbonnementComponent},
  { 
    path: 'accueil', 
    component: AccueilComponent,
    data: {
      title: 'Tuyaubet',
      description: " le site qui récompense les INFORMATIONS CAPITALES dénichées par les parieurs.",
      ogUrl: '/accueil'
    }
  },
  { path: 'cgu', component: CguComponent},  
  { path: 'mentions-legales', component: MentionsLegalesComponent},  
  { path: 'contact', component: ContactComponent},  
  { path:'', component: AccueilComponent},
  { path:'not-found', component: FourOhFourComponent},
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    FormulaireInfoComponent,
    PourquoiComponent,
    AuthComponent,
    OfficeComponent,
    AbonnementComponent,
    AccueilComponent,
    PourquoiComponent,
    CguComponent,
    MentionsLegalesComponent,
    ContactComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { useHash: true}),
    FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    AuthService,
    UserService,
    NotificationService,
    AccueilComponent,
    AppComponent,
    Meta,
    SEOService
  ],
  bootstrap: [AppComponent]  
})
export class AppModule { }

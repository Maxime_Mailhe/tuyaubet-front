// import { Component, OnInit } from '@angular/core';
// import { NgForm } from '@angular/forms';
// import { Router } from '@angular/router';
// import { UserService } from '../services/user.service';

// @Component({
//   selector: 'app-creation-compte',
//   templateUrl: './creation-compte.component.html',
//   styleUrls: ['./creation-compte.component.scss']
// })
// export class CreationCompteComponent implements OnInit {

//   constructor(private userService: UserService,
//               private router: Router) { }

//   ngOnInit(): void {
//   }

//   onSubmit(form: NgForm) {
//     const email_utilisateur = form.value['email_utilisateur'];
//     const pseudo_utilisateur = form.value['pseudo_utilisateur'];
//     const mdp_utilisateur = form.value['mdp_utilisateur'];
//     const check_CGU = form.value['check_CGU'];
//     const check_majeur = form.value['check_majeur'];
//     this.userService.addUser(email_utilisateur,
//                              pseudo_utilisateur,
//                              mdp_utilisateur,
//                              check_CGU,
//                              check_majeur);
//     this.userService.saveUsersToServer();
//     this.router.navigate(['/info']);
//   }

// }

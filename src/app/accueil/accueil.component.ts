import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faArrowAltCircleRight} from '@fortawesome/free-solid-svg-icons';
import { Injectable } from '@angular/core';

@Injectable()


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  constructor(private router: Router,
              private app: AppComponent) { }

  ngOnInit() {
    setTimeout(() => {
      this.app.hideMenu();
    },0)
  }
  isShow= true;
  circleRight = faArrowAltCircleRight;

  redirectAuth() {
    this.router.navigate(['/auth']);
  }

  redirectInfo() {
    this.router.navigate(['/info']);
  }

  showTuyaux() {
    this.isShow = !this.isShow;
  }
  displayTuyaux() {
    this.isShow = true;
  }
  redirectMentions() {
    this.router.navigate(['/mentions-legales']);
  }
  redirectCGU() {
    this.router.navigate(['/cgu']);
  }
  redirectContact() {
    this.router.navigate(['/contact']);
  }
  redirectFAQ() {
    this.router.navigate(['/pourquoi']);
  }
}

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {

    constructor(private toastr: ToastrService) { }

    showSuccess(title,message) {
      this.toastr.success(title, message, {
        timeOut: 4000,
        closeButton: true,
        progressBar: true,
        extendedTimeOut: 1000,
        positionClass: "toast-top-center"
      });
  }
  showError(title,message) {
    this.toastr.error(title, message, {
      timeOut: 4000,
      closeButton: true,
      progressBar: true,
      extendedTimeOut: 1000,
      positionClass: "toast-top-center"
    });
}
}

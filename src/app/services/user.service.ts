import { NotificationService } from './notification.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

    constructor(private httpClient: HttpClient,
        private notificationService: NotificationService) { }

    url = 'https://tuyaubet.herokuapp.com';

    users = [];
    infos = [];
    emailUsers = [];

    addInfo(email_utilisateur: string,
        sport_info: string,
        evenement_info: string,
        commentaire_info: string,
        lien_info: string) {
        const infoObject = {
            email_utilisateur: '',
            sport_info: '',
            evenement_info: '',
            commentaire_info: '',
            lien_info: ''
        };
        infoObject.email_utilisateur = email_utilisateur;
        infoObject.sport_info = sport_info;
        infoObject.evenement_info = evenement_info;
        infoObject.commentaire_info = commentaire_info;
        infoObject.lien_info = lien_info;
        this.infos.splice(0);
        this.infos.push(infoObject);
        this.sendInfoFromUsers();
    }

    sendInfoFromUsers() {
        console.log("url: "+this.url);
        this.httpClient
            .post(this.url + '/api/sendInfoFromUsers', this.infos)
            .subscribe(
                () => {
                    this.notificationService.showSuccess("Merci! Notre équipe te recontactera prochainement.", "Information envoyée");
                },
                (error) => {
                    console.log("erreur: " + error);
                }
            );
    }
    // saveEmailUserToServer() {
    //     this.httpClient
    //         .post(this.url + '/api/saveEmailUserToServer', this.emailUsers)
    //         .subscribe(
    //             () => {
    //                 console.log("email conservé dans la bdd");
    //             },
    //             (error) => {
    //                 console.log("erreur: " + error);
    //             }
    //         );
    // }
}

// EN ATTENTE DE L EVOLUTION DU PROJET

    // getUsersToServer() {
    //     this.httpClient
    //     .get<any[]>(this.url+'api/users')
    //     .subscribe(
    //         (response) => {
    //             this.usersFromdb = response;
    //         },
    //         (error) => {
    //             console.log('erreur :'+error);
    //         }
    //     );
    // }
    // usersFromdb = [];

        // addUser(email_utilisateur: string,
    //         pseudo_utilisateur: string,
    //         mdp_utilisateur: string,
    //         check_CGU: string,
    //         check_majeur: string) 
    //         {
    //             const userObject = {
    //                 // id: 0,
    //                 email_utilisateur: '',
    //                 pseudo_utilisateur: '',
    //                 mdp_utilisateur: '',
    //                 check_CGU: '',
    //                 check_majeur: ''
    //             };
    //             userObject.email_utilisateur = email_utilisateur;
    //             userObject.pseudo_utilisateur = pseudo_utilisateur;
    //             userObject.mdp_utilisateur = mdp_utilisateur;
    //             userObject.check_CGU = check_CGU;
    //             userObject.check_majeur = check_majeur;
    //             // userObject.id = this.users[(this.users.length - 1)].id + 1;

    //             this.users.push(userObject); 
    // }
    // saveUsersToServer(pseudo_utilisateur) {
    //     this.httpClient
    //         .post(this.url +'/api/inscription', this.users)
    //         .subscribe(
    //             ()=> {
    //                 this.notificationService.showSuccess("inscription effectuée","bienvenue "+pseudo_utilisateur);
    //             },
    //             (error) => {
    //                 console.log("erreur: " + error);
    //             }
    //         );
    // }
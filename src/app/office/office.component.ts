import { NotificationService } from './../services/notification.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss']
})
export class OfficeComponent implements OnInit {

  usersFromdb: any[];

  constructor(private userService: UserService,
    private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    // this.userService.getUsersToServer();
    // this.usersFromdb = this.userService.usersFromdb
  }

  // onDelete(id: number) {
  //   this.userService.users.splice(id, 1);
  // }

  // mailToUsers() {
  // }
}

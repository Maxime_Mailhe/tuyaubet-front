import { SEOService } from './services/SEO.service';
import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { Injectable } from '@angular/core';
import { filter, map, mergeMap } from 'rxjs/operators';

@Injectable()


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private router: Router,
              private seoService: SEOService,
              private activatedRoute: ActivatedRoute
              ) { }

  ngOnInit() {
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
     )
     .subscribe((event) => {
       this.seoService.updateTitle(event['title']);
       this.seoService.updateOgUrl(event['ogUrl']);
       //Updating Description tag dynamically with title
       this.seoService.updateDescription(event['description'])
     }); 
  }
  check: any;
  isShow = true;
  is_auth = false;
  title: any;
  menuOpen = false;
  user = faUser;
  // burgerChecked = document.querySelector("input").checked;

  redirectAuth() {
    this.router.navigate(['/auth']);
  }
burger(e){
  if(e.target.checked==true){
    this.check=true;
  }
}

showMenu(){
  this.isShow = true;
}


  hideMenu() {
    this.check = false;
  }
}

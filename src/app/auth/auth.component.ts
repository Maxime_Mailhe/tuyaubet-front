import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  user = new User();

  authStatus: boolean;

  constructor(private authService: AuthService,
              private userService: UserService,
              private http: HttpClient,
              private router: Router) { }

  ngOnInit(): void {
    this.authStatus = this.authService.isAuth;
  }

  onSubmit(form: NgForm) {
    // const email_utilisateur = form.value['email_utilisateur'];
    // const pseudo_utilisateur = form.value['pseudo_utilisateur'];
    // const mdp_utilisateur = form.value['mdp_utilisateur'];
    // const check_CGU = form.value['check_CGU'];
    // const check_majeur = form.value['check_majeur'];
    // this.userService.addUser(email_utilisateur,
    //                          pseudo_utilisateur,
    //                          mdp_utilisateur,
    //                          check_CGU,
    //                          check_majeur);
    // this.userService.saveUsersToServer(pseudo_utilisateur);
    // this.router.navigate(['/abonnement']);
  }

  onSignIn() {
    console.log(this.user);
    this.http.post('http://localhost:4242/auth',this.user).subscribe();
    // this.authService.signIn().then(
    //   ()=> {
    //     console.log("connexion réussie");
    //     this.authStatus = this.authService.isAuth;
    //   }
    // );
  }

  onSignOut() {
    this.authService.signOut();
    this.authStatus = this.authService.isAuth;
  }

}

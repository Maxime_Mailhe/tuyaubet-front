import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(private app: AppComponent) { }

  ngOnInit() {
    setTimeout(() => {
      this.app.hideMenu();
    },0)
  }

}

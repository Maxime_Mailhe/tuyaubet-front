import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pourquoi',
  templateUrl: './pourquoi.component.html',
  styleUrls: ['./pourquoi.component.scss']
})
export class PourquoiComponent implements OnInit {

  constructor(private app: AppComponent) { }

  ngOnInit() {
    setTimeout(() => {
      this.app.hideMenu();
    },0)
  }
  
  isShow1 = true;
  isShow2 = true;
  isShow3 = true;
  isShow4 = true;
  isShow5 = true;
  isShow6 = true;
  isShow7 = true;
  isShow8 = true;
  toggleDisplay1() {
    this.isShow1 = !this.isShow1;
  }
  toggleDisplay2() {
    this.isShow2 = !this.isShow2;
  }
  toggleDisplay3() {
    this.isShow3 = !this.isShow3;
  }
  toggleDisplay4() {
    this.isShow4 = !this.isShow4;
  }
  toggleDisplay5() {
    this.isShow5 = !this.isShow5;
  }
  toggleDisplay6() {
    this.isShow6 = !this.isShow6;
  }
  toggleDisplay7() {
    this.isShow7 = !this.isShow7;
  }
  toggleDisplay8() {
    this.isShow8 = !this.isShow8;
  }

}
